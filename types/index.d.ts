export type ShowType = {
    id: number;
    name: string;
    summary: string;
    image: string;
    imageOriginal: string;
    rating: number | null;
};

export type RootStackParamList = {
    Home: undefined;
    Details: undefined;
  };
