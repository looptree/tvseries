/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {
  SafeAreaView,
  View,
  Text,
  StyleSheet,
  Alert,
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';



import { ShowType, RootStackParamList } from './types';
import { queryShows } from './services/shows';
import { SearchBar } from './components/SearchBar/SearchBar';
import { List } from './components/List/List';

import { HomeScreen } from './screens/HomeScreen';
import { DetailsScreen } from './screens/DetailsScreen';

const Stack = createNativeStackNavigator<RootStackParamList>();

const App = () => {

  const isMounted = React.useRef(false);
  const [searchResult, setSearchResult] = React.useState([] as ShowType[]);
  const [show, setShow] = React.useState({} as ShowType);
  const [isSearching, setIsSearching] = React.useState(false);

  React.useEffect(() => {
    isMounted.current = true;
    return () => {
      isMounted.current = false;
    }
  }, []);

  const handleSearch = async (searchTerm: string) => {
    setIsSearching(true);
    try {
      const result = await queryShows(searchTerm);
      isMounted.current && setSearchResult(result);
    } catch (error) {
      Alert.alert(error);
    }
    isMounted.current && setIsSearching(false);
  };

  const handleReset = () => {
    if (searchResult) {
      setSearchResult([]);
    }
  };

  const Stack = createNativeStackNavigator();

  const Home = <HomeScreen
    handleSearch={handleSearch}
    handleReset={handleReset}
    data={searchResult}
    setShow={setShow}
    isSearching={isSearching}
  />;
  const Details = <DetailsScreen show={show} />

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          options={{
            headerShown: false,
          }}
        >
          {
            () => Home
          }
        </Stack.Screen>
        <Stack.Screen
          name="Details"
          options={{
            headerBackTitleVisible: false,
            headerTintColor: 'white',
            title: show.name,
            headerStyle: {
              backgroundColor: '#222',
            },
            headerTitleStyle: {
              color: '#eee',
            },
          }}
        >
          {
            () => Details
          }
        </Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
};


export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});



