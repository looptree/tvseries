import * as React from 'react';
import { ShowType } from './services/shows';

const initialState: ShowType[] = [];

export const QueryContext = React.createContext<ShowType[]>(
    initialState as ShowType[]
);

export const QueryContextProvider = ({
    children
}: {
    children: React.ReactNode;
}) => {
    const [shows, setShows] = React.useState(initialState as ShowType[]);
    return (
        <QueryContext.Provider
            value={{
                ...shows,
                setShows
            }}
        >
            {children}
        </QueryContext.Provider>
    )
}