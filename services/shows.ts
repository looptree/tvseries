import { ShowType } from '../types';
const axios = require('axios').default;

export const queryShows = async (query: string): Promise<ShowType[]> => {

    const shows: ShowType[] = [] as ShowType[];

    let response: any = {};
    try {
        response = await axios({
            method: 'get',
            url: `https://api.tvmaze.com/search/shows?q=${query}`,
            timeout: 10000,
        });
    } catch(error) {
        if (error.response) {
            throw "The TV Maze API might be down. Please try again later.";
        }
        if (error.request) {
            throw "Please check your internet connection.";
        }
        throw "Could not search.";
    }

    const { data } = response;
    if (data && data.length > 0) {
        data.forEach((item: any) => {
            const show = item.show;
            shows.push({
                id: show.id,
                name: show.name,
                summary: show.summary,
                image: show.image?.medium,
                imageOriginal: show.image?.original,
                rating: show.rating.average,
            });
        });
    }

    return Promise.resolve(shows);
}

