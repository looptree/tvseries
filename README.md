# TV Series mobile app

A React Native app using the TypeScript template [react-native-template-typescript](https://github.com/react-native-community/react-native-template-typescript).

### Before running
Download npm dependencies. In project directory run:

```sh
npm install
```

For iOS, run the following from the project directory:
```sh
npx pod-install
```

### Running the app
From the project directory, run:

#### iOS
1. Run the packager
    ```sh
    npm run start
    ```

2. Open the workspace in Xcode from `ios/TVSeries.xcworkspace`
3. Build and run from Xcode.

#### Android
1. Start an emulator or connect a physical device.
2. Run the packager
    ```
    npm run start
    ```

3. Build and run
    ```sh
    npx react-native run-android
    ```

