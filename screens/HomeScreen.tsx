import React from 'react';
 import {
   SafeAreaView,
   View,
   StyleSheet,
   StatusBar,
   ActivityIndicator,
 } from 'react-native';
 import { NativeStackNavigationProp } from '@react-navigation/native-stack';
 import { useNavigation } from '@react-navigation/native';
 import { ShowType, RootStackParamList } from '../types';
 import { SearchBar } from '../components/SearchBar/SearchBar';
 import { List } from '../components/List/List';

 type homeScreenProp = NativeStackNavigationProp<RootStackParamList, 'Home'>;

 export const HomeScreen = ({ handleSearch, handleReset, data, setShow, isSearching}: { handleSearch: any, handleReset: any, data: any, setShow: any, isSearching: boolean}) => {

    const navigation = useNavigation<homeScreenProp>();

    const handleSelectShow = (id: number) => {
      const show: ShowType | undefined = data.find(
          (el: { id: number; }) => 
            el.id === id
        );
      setShow(show);
      if (show) {
        navigation.navigate('Details');
      }
    };

    return (
    <SafeAreaView style={styles.container}>
        <StatusBar barStyle={'light-content'} backgroundColor="#61dafb" />
        <View style={{ 
            paddingTop: 15, 
            backgroundColor: '#333',
            paddingBottom: 15,
            paddingHorizontal: 20,
        }}>
        <SearchBar 
            onSearch={handleSearch}
            onReset={handleReset}
        />
        </View>
        
        {isSearching 
            ? 
            <View style={styles.activity}>
                <ActivityIndicator />
            </View>
            :
            <List
                data={data}
                onPress={handleSelectShow}
            />
        }
        
      </SafeAreaView>
    );
  };

  const styles = StyleSheet.create({
    container: {
      flex: 1, 
      backgroundColor: 'black',
    },
    activity: {
        marginTop: 50,
    },
  });
 
  
