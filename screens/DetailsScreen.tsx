import React from 'react';
import { SafeAreaView, StyleSheet, Text } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { ShowType, RootStackParamList } from '../types';
import { ShowDetails } from '../components/ShowDetails/ShowDetails';

type detailsScreenProp = NativeStackNavigationProp<RootStackParamList, 'Details'>;

export const DetailsScreen = ({ show }: { show: ShowType }) => {

    const navigation = useNavigation<detailsScreenProp>();
    
    return (
        <SafeAreaView style={styles.container}>
            <ShowDetails
                show={show}
            />
        
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
container: {
    flex: 1,
    paddingHorizontal: 10,
    backgroundColor: 'black',
},
});