import * as React from 'react';
import { TextInput, View, StyleSheet, TouchableOpacity, Image } from 'react-native';
const iconClear = require('./outline_clear_black_24dp.png');

const ResetButton = ({ onPress }: { onPress: any }) => {
    return (
        <TouchableOpacity
            style={styles.resetButton}
            onPress={onPress}
        >
            <View style={styles.resetIconContainer}>
                <Image
                    source={iconClear}
                    style={styles.resetIcon}
                />
            </View>
        </TouchableOpacity>
    );
};

export const SearchBar = ({ onSearch, onReset }: { onSearch: any, onReset: any }) => {

        const [value, setValue] = React.useState("");
        const handleChangeText = (value: string) => {
            setValue(value);
        };

        const handleSearch = () => {
            onSearch(value);
        }

        const handleReset = () => {
            setValue("");
            onReset();
        };

        return (
            <View
                style={[
                    styles.container,
                ]}>

                <TextInput
                    value={value}
                    placeholder="Search TV show..."
                    placeholderTextColor='#bbb'
                    style={styles.input}
                    returnKeyType='search'
                    autoCorrect={false}
                    onChangeText={handleChangeText}
                    onSubmitEditing={handleSearch}
                />

                {
                    value ?
                        <ResetButton
                            onPress={handleReset}
                        />
                        : null
                }

            </View>
        );
    };

const styles = StyleSheet.create({
    container: {
        height: 50,
        backgroundColor: '#555',
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
    },
    input: {
        height: 50,
        flex: 1,
        marginLeft: 20,
        color: 'white',
    },
    resetButton: {
        width: 50,
        height: 50,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    resetIconContainer: {
        width: 24,
        height: 24,
        backgroundColor: 'white',
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center',
    },
    resetIcon: {
        width: 16,
        height: 16,
    },
});
