import React from 'react';
import { FlatList, StyleSheet } from 'react-native';

import { ShowType } from '../../types';
import { ListItem } from '../ListItem/ListItem';

export const List = ({ data, onPress }: { data: ShowType[], onPress: any }) => {

    const renderItem = ({ item, index }: { item: ShowType, index: number }) => (
        <ListItem
            id={item.id}
            title={item.name}
            image={item.image}
            onPress={onPress}
            first={index === 0}
        />
    );

    return (
        <FlatList
            data={data}
            renderItem={renderItem}
            style={styles.list}
            keyboardShouldPersistTaps='always'
            contentContainerStyle={styles.container}
        />
    );
};

const styles = StyleSheet.create({
    container: {
        paddingTop: 35,
    },
    list: {
        paddingHorizontal: 20,
    },
});