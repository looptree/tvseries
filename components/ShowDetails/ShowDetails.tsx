import React from 'react';
import { StyleSheet, ScrollView, View, Image, Text, useWindowDimensions } from 'react-native';
import RenderHtml from 'react-native-render-html';
import { ShowType } from '../../types';
import { Rating } from './Rating';

export const ShowDetails = ({ show }: { show: ShowType }) => {
  
    const { width } = useWindowDimensions();
    const imgWidth = width / 3;
    const ratio = 1.4;
    
    return (
        <ScrollView
            contentContainerStyle={styles.container}
        >
            <View style={{ flexDirection: 'row' }}>
                <Image
                    style={[styles.image, { width: imgWidth, height: imgWidth*ratio }]}
                    source={{ uri: show.image }}
                />
                <View style={{ justifyContent: 'center' }}>
                    <Rating
                        value={show.rating}
                    />
                </View>
            </View>

            <Text style={styles.title}>{show.name}</Text>

            {show.summary ?
                <RenderHtml
                    contentWidth={width}
                    source={{ html: show.summary }}
                    tagsStyles={{ p: {
                        fontSize: 18,
                        color: '#ddd',
                    }}}
                />
                :
                <Text style={[styles.summary, styles.note]}>
                    No summary available.
                </Text>
            }

        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'black',
        padding: 8,
        paddingTop: 20,
    },
    title: {
        color: 'white',
        fontSize: 24,
        marginTop: 15,
        marginBottom: -10,
    },
    summary: {
        fontSize: 20,
    },
    note: {
        marginTop: 10,
        color: 'yellow',
    },
    image: {
        width: 200,
        height: 200,
        backgroundColor: 'gray',
    },
});
