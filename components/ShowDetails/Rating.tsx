import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
const iconStar   = require('./outline_star_white_48dp.png');

export const Rating = ({ value }: { value: number | null }) => {
  
    return (
        <View style={styles.container}>
            <Image 
                style={[styles.image, !value ? styles.disabled : null]}
                source={iconStar}
            />
            <Text 
                style={[styles.text, !value ? styles.disabledText : null]}
            >
                {value ? value : "Rating not available"}
            </Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        padding: 20,
    },
   image: {
       width: 24,
       height: 24,
   },
   disabled: {
        opacity: 0.5,
   },
   text: {
       color: 'white',
       fontSize: 20,
       fontWeight: 'bold',
   },
   disabledText: {
    fontSize: 15,
   },
});
