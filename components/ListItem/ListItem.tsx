import React from 'react';
import { StyleSheet, Text, Image, TouchableOpacity, useWindowDimensions } from 'react-native';

export const ListItem = ({ id, title, image, onPress, first }: { id: number, title: string, image: string, onPress: any, first: boolean }) => {

    const { width } = useWindowDimensions();
    const imgWidth = width / 6;
    const ratio = 1.4;
    
    const handlePress = () => {
        onPress(id);
    };

    return (
        <TouchableOpacity 
            style={[styles.item, first && styles.first]}
            onPress={handlePress}    
        >
            <Image
                style={[styles.image, { width: imgWidth, height: imgWidth * ratio }]}
                source={{ uri: image }}
            />
            <Text style={styles.title}>{title}</Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    item: {
        backgroundColor: 'black',
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderColor: '#555',
        flexDirection: 'row',
        alignItems: 'center',
    },
    first: {
        borderTopWidth: 1,
    },
    title: {
        fontSize: 20,
        flex: 1,
        flexWrap: 'wrap',
        color: 'white',
    },
    image: {
        width: 40,
        height: 40,
        marginRight: 10,
        backgroundColor: 'gray',
    },
});
